Projet n°6 pour OpenClassrooms

- Licence : License libre
- Auteur : Jérôme Deneux

---------------------
Description du projet
---------------------

Site réalisé pour une association fictive d'escalade dans le cadre d'un projet.

Cahier des charges du client : https://s3-eu-west-1.amazonaws.com/course.oc-static.com/projects/DAJava_P6/Recueil+des+besoins+client+-+Amis+de+l'Escalade.pdf

----------
Pré-requis
----------

Vous devez posséder Java JRE version 8 ou supérieur sur votre machine pour pouvoir correctement utiliser l'application.
 
----------------------
Technologies employées
----------------------
Langage :
- Java 8
- TypeScript

Front : 
- Angular

Back :
- Spring Data
- Spring Web
- Spring Security
- JWT
- Swagger
- Lombok


Base de données :
- PostgreSQL

Serveur d'application :
Apache Tomcat v.9
